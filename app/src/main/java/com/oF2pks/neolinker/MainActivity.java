package com.oF2pks.neolinker;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.Toast;



//import org.apache.http.HttpVersion;
public class MainActivity extends Activity {
    public static final String RECURS = "RECURS";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        if (intent != null && intent.getAction()!= null) {

            if (intent.getAction().equals(Intent.ACTION_SEND)) {

                String urlText = intent.getStringExtra(Intent.EXTRA_TEXT);
                Uri uri = findUrlInString(urlText);

                if(uri != null && intent.getStringExtra(MainActivity.RECURS) == null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setDataAndType(uri,"text/plain");
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    i.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY);
                    if(intent.getStringExtra(Intent.EXTRA_SUBJECT)!=null
                            && intent.getStringExtra(Intent.EXTRA_SUBJECT).length()>0)
                        i.putExtra(RECURS, intent.getStringExtra(Intent.EXTRA_SUBJECT));
                    else i.putExtra(RECURS, "1");
                    startActivity(Intent.createChooser(i, urlText));
                }else if(intent.getStringExtra(Intent.EXTRA_SUBJECT) != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setDataAndType(Uri.parse(getString(R.string.q1)
                                    + urlQ(intent.getStringExtra(Intent.EXTRA_SUBJECT)))
                            , "text/plain");
                    i.putExtra(RECURS, "2");
                    startActivity(Intent.createChooser(i, getString(R.string.i1) + intent.getStringExtra(Intent.EXTRA_SUBJECT)));
                }else if(intent.getStringExtra(Intent.EXTRA_TEXT) != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setDataAndType(Uri.parse(getString(R.string.q1)
                                    + urlQ(intent.getStringExtra(Intent.EXTRA_TEXT)))
                            , "text/plain");
                    i.putExtra(RECURS, "2");
                    startActivity(Intent.createChooser(i, getString(R.string.i1) + intent.getStringExtra(Intent.EXTRA_TEXT)));

                }else {
                    Toast.makeText(this, getString(R.string.error), Toast.LENGTH_LONG).show();
                }

            }else if (intent.getAction().equals(Intent.ACTION_VIEW) && intent.getData() != null){
                String s=getIntent().getStringExtra(MainActivity.RECURS)==null
                        ? "0"
                        :getIntent().getStringExtra(MainActivity.RECURS);
                Intent i = new Intent();
                switch (s) {
                    case "0":
                        i.setAction(Intent.ACTION_VIEW);
                        i.setData(intent.getData());
                        i.putExtra(RECURS, "+");
                        startActivity(Intent.createChooser(i, "www: " + intent.getData().toString().replace("%20"," ")));
                        break;
                    case "+":
                        i.setAction(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(RECURS, "1");
                        i.putExtra(Intent.EXTRA_TEXT, intent.getDataString());
                        startActivity(Intent.createChooser(i, "Share: " + intent.getData().toString().replace("%20"," ")));
                        break;
                    case "1":
                        i.setAction(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(getString(R.string.q1)
                                +urlQ(intent.getData().toString())));
                        i.putExtra(RECURS, "2");
                        startActivity(Intent.createChooser(i, getString(R.string.i1) + intent.getData().toString().replace("%20"," ")));
                        break;
                    case "2":
                        i.setAction(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(intent.getData().toString().replace(getString(R.string.j1),"gibiru.com/results.html")));
                        i.putExtra(RECURS, "4");
                        startActivity(Intent.createChooser(i, "GIBIRU: " + i.getData().toString().replace("%20"," ")));
                        break;
                    case "4":
                        i.setAction(Intent.ACTION_VIEW);
                        if (intent.getDataString()!= null && intent.getDataString().split("/").length>5) {
                            i.setData(Uri.parse("https://www.virustotal.com/gui/domain/"
                                    + intent.getDataString().split("/")[5]
                                    + "/relations"));
                            i.putExtra(RECURS, "5");
                            startActivity(Intent.createChooser(i, "VIRUSTOTAL: " + intent.getDataString().split("/")[5]));
                        }
                        break;
                    default:
                        if (s.length()<=1) break;
                        i.setAction(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(getString(R.string.q1)+urlQ(s)));
                        i.putExtra(RECURS, "2");
                        startActivity(Intent.createChooser(i, getString(R.string.i1) + s.replace("%20"," ")));

                }

            }else if (Build.VERSION.SDK_INT >22
                    && intent.getAction().equals(Intent.ACTION_PROCESS_TEXT)
                    && intent.getStringExtra(Intent.EXTRA_PROCESS_TEXT) != null
                    && getIntent().getStringExtra(MainActivity.RECURS) == null){
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(getString(R.string.q1)
                        +urlQ(intent.getStringExtra(Intent.EXTRA_PROCESS_TEXT))));
                i.putExtra(RECURS, "2");
                startActivity(Intent.createChooser(i, getString(R.string.i1) + intent.getStringExtra(Intent.EXTRA_PROCESS_TEXT)));

            }else if (intent.getAction().equals(Intent.ACTION_WEB_SEARCH)
                    && intent.getStringExtra(SearchManager.QUERY) != null
                    && getIntent().getStringExtra(MainActivity.RECURS) == null){
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(getString(R.string.q1)
                        +urlQ(intent.getStringExtra(SearchManager.QUERY))));
                i.putExtra(RECURS, "2");
                startActivity(Intent.createChooser(i, getString(R.string.i1) + intent.getStringExtra(SearchManager.QUERY)));
            }
    	}
        Toast toast = Toast.makeText(this,"avoid: Open with ... \n\t\t<always>", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP| Gravity.END, 0, 0);
        toast.show();
        finish();
    }

    private Uri findUrlInString(String urlText) {
        if (urlText == null) return null;
        int indexOfUrl = urlText.toLowerCase().indexOf("http");//HttpVersion.HTTP.toLowerCase());
        if(indexOfUrl == -1)
            return null;
        else{
            String containsURL = urlText.substring(indexOfUrl);

            int endOfUrl = containsURL.indexOf(" ");

            String url;
            if(endOfUrl != -1){
                url = containsURL.substring(0, endOfUrl);
            }else{
                url = containsURL;
            }

            return Uri.parse(url);
        }
    }
    private String urlQ(String s){
        return s.replace(" ","%20").replace("\n",".").replace("\"","");
    }
}

