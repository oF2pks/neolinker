## Hybrid version of Flashify & browser-intercept 
(without any permission or security weakness) 

### Flashify 
Open websites in another browser
> ...is a simple utility app that takes a url from the Share Intent in Android and opens it in another browser. It was orginally designed to easily open Adobe Flash websites in a browser that supports Flash but is equally useful for quickly testing mobile websites in multiple browsers.

https://github.com/GodsMoon/Flashify

### browser-intercept 
shows url & choice of browsers before launching links.

https://github.com/intrications/browser-intercept


### Iterate <createChooser> on NeoLinker icon 
will switch to:

 1. -> @view/browse: link
 1. -> @SEND/share: {link}
 1. -> @view/browse: searX{link}
 1. -> @view/browse: Gibiru{link}
 1. -> @view/browse: VirusTotal{domain(link)}

(included: sdk29 + @PROCESS_TEXT(Lollipop api22) & @WEB_SEARCH)
